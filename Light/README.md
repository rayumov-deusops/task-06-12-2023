** Устанавливаем VAGRANT

1) Поднимаем 2 виртуалки
vagrant up

2) Переходим в files виртуалку
vagrant ssh vm2

3) Устанавливаем NFS сервер
sudo apt update
sudo apt install nfs-kernel-server -y

4) Создаем директорию для шаринга
sudo mkdir -p /mnt/nfsdir

5) Change the owner user and group to nobody and nogroup. This setting makes the folder public:
sudo chown nobody:nogroup /mnt/nfsdir

6) Set permissions to 777, so everyone can read, write, and execute files in this folder:
sudo chmod 777 /mnt/nfsdir

7) Добавляем в /etc/exports
sudo nano /etc/exports

/mnt/nfsdir 192.168.50.10/0(rw,sync,no_subtree_check,insecure)

8) Посмотреть Shared Directory
sudo exportfs -a

8.1) Посмотреть смонтированные директории
showmount -e

*Export list for files01:
/mnt/nfsdir 192.168.50.10/0

9) Рестартуем NFS Kernel Server для применения изменений
sudo systemctl restart nfs-kernel-server

*10) If you use UFW, you need to allow clients to access the server:
sudo ufw allow from [clientIP or clientSubnetIP] to any port nfs
sudo ufw allow from 10.0.1.3 to any port nfs
или
sudo ufw allow to any port nfs

11) To make sure you successfully completed the operation, type:
sudo ufw status

11.1) Смотрим ip виртуалки и запоминаем:
ip addr

12) Завершаем сессию с виртуалкой
logout
----------------------------------------------------------------------

12) Подключаемся ко второй виртуалке (web01)
vagrant ssh vm1

13) Устанавливаем NFS клиент
sudo apt update
sudo apt install nfs-common -y

14) Создаем директорию:
sudo mkdir -p /mnt/nfsdir_client

15) Mound directory
sudo mount 192.168.50.11:/mnt/nfsdir /mnt/nfsdir_client

16) Проверяем
df -h
192.168.50.11:/mnt/nfsdir   39G  1.6G   38G   4% /mnt/nfsdir_client

17) If you want the folders to stay mounted even after you restart the machine, you will need to add them to the /etc/fstab file.
sudo nano /etc/fstab

192.168.50.11:/mnt/nfsdir /mnt/nfsdir_client nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0

18) Устанавливаем nginx
sudo apt install nginx -y

19) Меняем default конфиг
sudo nano /etc/nginx/sites-available/default

server {
    server_name myproxy;
    listen 80;

    location / {
        proxy_pass http://127.0.0.1:8000;
    }
    
    location /media/ {
        alias /mnt/nfsdir_client;
    }
} 

20) Рестартуем nginx
sudo systemctl restart nginx.service

21) Устанавливаем python и запускаем приложение
sudo apt-get install -y python3-pip
sudo pip3 install django
sudo apt-get install -y python3-venv
sudo pip install djangorestframework whitenoise
sudo python3 -m venv venv
. venv/bin/activate

22) Меняем директория для загружаемых файлов
nano filesharing/settings.py

MEDIA_ROOT = '/mnt/nfsdir_client'
MEDIA_URL = '/media/'

23) Запускаем приложение
sudo python3 ./manage.py runserver