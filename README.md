# Моя первая задача от [Deusops](https://deusops.com/)

# Условия и ссылки
<details>
<summary>Light:</summary>

1. Развернуть две виртуальные машины с Linux: web01 и files01. Вы можете использовать любые средства, например, Яндекс.Облако или локальный Vagrant.
2. Установить NFS-сервер на виртуальной машине "files01". Настроить экспорт директории "/files".
3. Установить NFS-клиент на виртуальной машине "web01". Настроить подключение к серверу и получить директорию с сервера.
4. Установить и настроить Nginx на виртуальной машине "web01". Настроить сервер в качестве прокси на localhost.
5. Установить Django на "web01", запустить Django-приложение и проверить, что всё работает.
6. Настроить сервер так, чтобы файлы, загруженные через веб-сайт, оказывались в хранилище NFS.

</details>

<details>
<summary>Normal:</summary>

1. Написать Dockerfile для приложения, установить Docker на "web01" и запустить приложение в контейнере. Убедиться, что всё работает так, как настраивали в уровне Light.
2. Написать GitLab CI Pipeline для автоматической сборки и публикации Docker-образа с приложением.
3. Написать Ansible playbook, который настраивает серверы "web01" и "files01".
4. Написать Ansible-роль "nfs" для установки NFS-сервера и NFS-клиента. Придумать, как разделять Server и Client на уровне ролей и в файле inventory-hosts.
5. Написать Ansible-роль "nginx" для установки Nginx и настройки прокси на localhost.
6. Написать Ansible-роль "docker" для установки Docker и шаблона для запуска Docker Compose с нужным приложением.
7. Добавить в GitLab CI шаг с деплоем приложения на сервера. Ansible-роли должны находиться в отдельных репозиториях и использовать Ansible Galaxy.

</details>

<details>
<summary>Hard:</summary>

1. Развернуть в облаке Kubernetes-кластер и настроить Ingress для доступа извне.
2. Используя созданные ранее Ansible-роли, настроить виртуальную машину для NFS-сервера.
3. Подключить в Kubernetes nfs-subdir-external-provisioner, настроить работу волумов через NFS.
4. Создать Helm-чарт для деплоя приложения в Kubernetes-кластер.
5. Доработать Pipeline для автоматического развертывания приложения в Kubernetes-кластер.
6. Вынести инфраструктурные элементы IAC из репозитория проекта в инфраструктурный репозиторий.

</details>

<details>
<summary>Expert:</summary>

1. Создать документацию с описанием архитектуры, настроек и процедур обслуживания для вашей инфраструктуры.
2. Продумать поток доставки приложения на контура и разработать GitLab CI Pipeline для автоматического развертывания инфраструктуры.
3. Оформить развертывание всей инфраструктуры в Яндекс Облаке через Terraform. Оставить провижен виртуальных машин под управлением Ansible.

</details>

<details>
<summary>Links:</summary>

- [Как поднимать виртуальные машины в Vagrant](https://gitlab.com/deusops/lessons/documentation/vagrant)
- [Wikipedia - NFS](https://ru.wikipedia.org/wiki/Network_File_System)
- [File Storage Types and Protocols for Beginners](https://www.youtube.com/watch?v=ABQ7okl09RY)
- [Ubuntu Wiki - NFS](https://help.ubuntu.ru/wiki/nfs)
- [Документация по Docker](https://gitlab.com/deusops/lessons/documentation/docker)
- [Документация по GitLab CI](https://gitlab.com/deusops/lessons/documentation/gitlab-ci)
- [Гайд по ролям в Ansible](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)
- [Гайд по Galaxy в Ansible](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html)
- [NFS в качестве PersistentVolume](https://www.digitalocean.com/community/tutorials/how-to-set-up-readwritemany-rwx-persistent-volumes-with-nfs-on-digitalocean-kubernetes-ru)
- [Kubernetes: StorageClasses](https://kubernetes.io/docs/concepts/storage/storage-classes/)
- [PV в Kubernetes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)
- [PV и PVC в Kubernetes](https://rtfm.co.ua/kubernetes-persistentvolume-i-persistentvolumeclaim-obzor-i-primery/)
- [Django-приложение](https://gitfront.io/r/deusops/BC6tmrogTrbh/django-filesharing/)
- [NFS Subdir External Provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)
</details>