[Репозиторий с приложением](https://gitlab.com/va1erify-deusops/task-06-12-2023-app-nfs)\
[Инфраструктурный репозиторий](https://gitlab.com/va1erify-deusops/task-06-12-2023-infrastructure)\
[Docker-образ с Ansible](https://gitlab.com/va1erify-deusops/docker-image-with-ansible)\
[Docker-образ с Terraform](https://gitlab.com/va1erify-deusops/docker-image-with-terraform)\
[Ansible роль - Установка Docker](https://gitlab.com/va1erify-ansible-roles/ansible-role-docker)\
[Ansible роль - Установка Nginx](https://gitlab.com/va1erify-ansible-roles/ansible-role-nginx)


## Gitlab CI/CD vars

| Type     | Key | Description                                                    |
|----------| --- |----------------------------------------------------------------|
| File     | NFS_CLIENT_PRIVATE_KEY | Masked Private SSH ключ для деплоя (закодированный через base64) |
| File     | NFS_SERVER_PRIVATE_KEY | Masked Private SSH ключ для деплоя (закодированный через base64) |
| Variable | NFS_CLIENT_EXTERNAL_IP | IP адрес |
| Variable | NFS_SERVER_EXTERNAL_IP | IP адрес |
