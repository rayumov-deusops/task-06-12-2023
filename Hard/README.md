## Репозиторий для установки Приложения в K8s

Этот репозиторий содержит CI/CD пайплайн для автоматизации процесса установки приложения в кластер K8s с использованием
Helm.

## Gitlab CI/CD переменные

| Type     | Key              | Description                                                                                                   |
|----------|------------------|---------------------------------------------------------------------------------------------------------------|
| VARIABLE | ACCES_TOKEN      | Access token для доступа к репозиторию с приложением (Masked)                                                 |
| VARIABLE | USERNAME         | Имя пользователя для доступа к репозиторию с приложением                                                      |
| VARIABLE | PROJECT_ID       | ID репозитория с приложением                                                                                  |
| VARIABLE | K8S_CLUSTER_NAME | Имя кластера в K8s                                                                                            |
| VARIABLE | CLOUD_ID         | Yandex cloud ID. Used as a variable in the Terraform pipeline to identify the Yandex Cloud ID                 |
| VARIABLE | FOLDER_ID        | Yandex folder ID. Used as a variable in the Terraform pipeline to specify the target folder in Yandex Cloud   |
| VARIABLE | TOKEN            | Yandex token. Used as a variable in the Terraform pipeline to authenticate with the Yandex Cloud API (Masked) |

## Переменные Пайплайна

- **APP_VERSION**: Определяет версию приложения для установки (например, 1.0.0).
- **IMAGE**: Ссылка на образ, в котором будет исполняться пайплайн.
- **HELM_REPO_NAME**: Название Helm репозитория (какое будет "name" при "helm repo list")
- **PACKAGE_NAME**: Название пакета в Package Registry
- **HELM_CHART_NAME**: Под каким названием chart будет задеплоен в k8s.

## Шаги Выполнения Пайплайна

1. **Настройка Yandex Cloud CLI**:
    - Создание профиля Yandex Cloud CLI.
    - Настройка необходимых параметров, таких как идентификатор папки (folder ID), идентификатор облака (cloud ID) и
      токен.

2. **Получение Учетных Данных Кластера Kubernetes**:
    - Получение учетных данных для указанного кластера Kubernetes.

3. **Добавление Репозитория Helm**:
    - Добавление репозитория Helm для приложения с использованием предоставленных учетных данных.
    - Обновление репозитория Helm для обеспечения наличия последних версий чартов.

4. **Установка Приложения с помощью Helm**:
    - Установка указанного приложения в кластер Kubernetes с использованием Helm.
    - Версия приложения определяется переменной `APP_VERSION`.

## Связанные репозитории

[Репозиторий с приложением](https://gitlab.com/va1erify-deusops/task-06-12-2023-app-nfs)\
[Инфраструктурный репозиторий](https://gitlab.com/va1erify-deusops/task-06-12-2023-infrastructure-k8s)\
[Docker-образ с Helm](https://gitlab.com/va1erify-deusops/docker-image-with-terraform-yandexcli-kubectl-helm)